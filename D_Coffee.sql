--
-- File generated with SQLiteStudio v3.2.1 on พฤ. ก.ย. 8 00:19:25 2022
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: category
DROP TABLE IF EXISTS category;

CREATE TABLE category (
    category_id   VARCHAR (5) UNIQUE
                              PRIMARY KEY
                              NOT NULL,
    category_name TEXT (50)   NOT NULL
);

INSERT INTO category (
                         category_id,
                         category_name
                     )
                     VALUES (
                         'C01',
                         'กาแฟ'
                     );

INSERT INTO category (
                         category_id,
                         category_name
                     )
                     VALUES (
                         'C02',
                         'ชา'
                     );

INSERT INTO category (
                         category_id,
                         category_name
                     )
                     VALUES (
                         'C03',
                         'น้ำผลไม้'
                     );

INSERT INTO category (
                         category_id,
                         category_name
                     )
                     VALUES (
                         'C04',
                         'นม โกโก้ และคาราเมล'
                     );

INSERT INTO category (
                         category_id,
                         category_name
                     )
                     VALUES (
                         'C05',
                         'สมูทตี้เพื่อสุขภาพ'
                     );

INSERT INTO category (
                         category_id,
                         category_name
                     )
                     VALUES (
                         'C06',
                         'ขนมหวาน'
                     );

INSERT INTO category (
                         category_id,
                         category_name
                     )
                     VALUES (
                         'C07',
                         'อาหาร'
                     );


-- Table: customer
DROP TABLE IF EXISTS customer;

CREATE TABLE customer (
    customer_id    VARCHAR (10) PRIMARY KEY
                                UNIQUE,
    customer_name  TEXT (50)    NOT NULL,
    customer_tel   VARCHAR (10) NOT NULL,
    customer_point INTEGER      NOT NULL
);

INSERT INTO customer (
                         customer_id,
                         customer_name,
                         customer_tel,
                         customer_point
                     )
                     VALUES (
                         'CM000001',
                         'นางลำไย หวาน',
                         '0998889988',
                         45
                     );

INSERT INTO customer (
                         customer_id,
                         customer_name,
                         customer_tel,
                         customer_point
                     )
                     VALUES (
                         'CM000002',
                         'นางหมูยอ อุบล',
                         '0877745386',
                         9
                     );

INSERT INTO customer (
                         customer_id,
                         customer_name,
                         customer_tel,
                         customer_point
                     )
                     VALUES (
                         'CM000003',
                         'นายแฮม มินตั่น',
                         '0995086978',
                         32
                     );

INSERT INTO customer (
                         customer_id,
                         customer_name,
                         customer_tel,
                         customer_point
                     )
                     VALUES (
                         'CM000004',
                         'นางสร้อย น้อย',
                         '0999999999',
                         400
                     );

INSERT INTO customer (
                         customer_id,
                         customer_name,
                         customer_tel,
                         customer_point
                     )
                     VALUES (
                         'CM000005',
                         'นางสาวน้อย หน่า',
                         '0885004238',
                         25
                     );

INSERT INTO customer (
                         customer_id,
                         customer_name,
                         customer_tel,
                         customer_point
                     )
                     VALUES (
                         'CM000006',
                         'นายน้อย จัย',
                         '0881002493',
                         65
                     );

INSERT INTO customer (
                         customer_id,
                         customer_name,
                         customer_tel,
                         customer_point
                     )
                     VALUES (
                         'CM000007',
                         'นายจัย บาง',
                         '0881002912',
                         12
                     );


-- Table: employee
DROP TABLE IF EXISTS employee;

CREATE TABLE employee (
    employee_id      VARCHAR (10) PRIMARY KEY
                                  UNIQUE
                                  NOT NULL,
    employee_name    TEXT (50)    NOT NULL,
    employee_address TEXT (100)   NOT NULL,
    employee_tel     VARCHAR (10) NOT NULL,
    employee_email   TEXT (50)    NOT NULL
);

INSERT INTO employee (
                         employee_id,
                         employee_name,
                         employee_address,
                         employee_tel,
                         employee_email
                     )
                     VALUES (
                         'EM000001',
                         'นายบุญ ดินดี',
                         '123 ต.บ้านบึง อ.บ้านบึง จ.ชลบุรี 20170',
                         '0623456789',
                         'boon@gmail.com'
                     );

INSERT INTO employee (
                         employee_id,
                         employee_name,
                         employee_address,
                         employee_tel,
                         employee_email
                     )
                     VALUES (
                         'EM000002',
                         'นางศรี อรทัย',
                         '44 ต.บ้านบึง อ.บ้านบึง จ.ชลบุรี 20170',
                         '0665455555',
                         'zee@gmail.com'
                     );

INSERT INTO employee (
                         employee_id,
                         employee_name,
                         employee_address,
                         employee_tel,
                         employee_email
                     )
                     VALUES (
                         'EM000003',
                         'นายธนดล บุญถึง',
                         '261 ต.แสนสุข อ.เมือง จ.ชลบุรี 20130',
                         '0638844262',
                         'thanadon373@outlook.com'
                     );

INSERT INTO employee (
                         employee_id,
                         employee_name,
                         employee_address,
                         employee_tel,
                         employee_email
                     )
                     VALUES (
                         'EM000004',
                         'นายกอ ขยันดี',
                         '446 ต.บ้านสวน อ.เมือง จ.ชลบุรี 20000',
                         '0644446666',
                         'gg@gmail.com'
                     );

INSERT INTO employee (
                         employee_id,
                         employee_name,
                         employee_address,
                         employee_tel,
                         employee_email
                     )
                     VALUES (
                         'EM000005',
                         'นายขอ สุริน',
                         '1/88 ต.แสนสุข อ.เมือง จ.ชลบุรี 20130',
                         '0894266264',
                         'kk@gmail.com'
                     );

INSERT INTO employee (
                         employee_id,
                         employee_name,
                         employee_address,
                         employee_tel,
                         employee_email
                     )
                     VALUES (
                         'EM000006',
                         'นางสาวคอ ขยันดี',
                         '111 ต.บ้านสวน อ.เมือง จ.ชลบุรี 20000',
                         '0801112233',
                         'ko@gmail.com'
                     );

INSERT INTO employee (
                         employee_id,
                         employee_name,
                         employee_address,
                         employee_tel,
                         employee_email
                     )
                     VALUES (
                         'EM000007',
                         'นางตัวดี สรญา',
                         '33/5 ต.บ้านสวน อ.เมือง จ.ชลบุรี 20000',
                         '0882245286',
                         'dd5@gmail.com'
                     );


-- Table: order
DROP TABLE IF EXISTS [order];

CREATE TABLE [order] (
    order_id             VARCHAR (10) PRIMARY KEY
                                      UNIQUE
                                      NOT NULL,
    order_queue          INTEGER      NOT NULL,
    order_date           DATE         NOT NULL,
    order_time           TIME         NOT NULL,
    [order_discount (%)] INTEGER      NOT NULL,
    order_total          DOUBLE       NOT NULL,
    order_recieved       INTEGER      NOT NULL,
    order_change         INTEGER      NOT NULL,
    order_payment        VARCHAR (2)  DEFAULT PC
                                      NOT NULL,
    store_id             VARCHAR (4)  REFERENCES store (store_id) ON DELETE CASCADE
                                                                  ON UPDATE CASCADE
                                      NOT NULL,
    employee_id          VARCHAR (10) REFERENCES employee (employee_id) ON DELETE CASCADE
                                                                        ON UPDATE CASCADE
                                      NOT NULL,
    customer_id          VARCHAR (10) REFERENCES customer (customer_id) ON DELETE CASCADE
                                                                        ON UPDATE CASCADE
);

INSERT INTO [order] (
                        order_id,
                        order_queue,
                        order_date,
                        order_time,
                        [order_discount (%)],
                        order_total,
                        order_recieved,
                        order_change,
                        order_payment,
                        store_id,
                        employee_id,
                        customer_id
                    )
                    VALUES (
                        'OR000001',
                        1,
                        '26/8/2022',
                        '7:30',
                        0,
                        90.0,
                        90,
                        0,
                        'P',
                        'S01',
                        'EM000003',
                        NULL
                    );

INSERT INTO [order] (
                        order_id,
                        order_queue,
                        order_date,
                        order_time,
                        [order_discount (%)],
                        order_total,
                        order_recieved,
                        order_change,
                        order_payment,
                        store_id,
                        employee_id,
                        customer_id
                    )
                    VALUES (
                        'OR000002',
                        2,
                        '26/8/2022',
                        '7:45',
                        5,
                        118.0,
                        118,
                        0,
                        'P',
                        'S01',
                        'EM000003',
                        'CM000004'
                    );

INSERT INTO [order] (
                        order_id,
                        order_queue,
                        order_date,
                        order_time,
                        [order_discount (%)],
                        order_total,
                        order_recieved,
                        order_change,
                        order_payment,
                        store_id,
                        employee_id,
                        customer_id
                    )
                    VALUES (
                        'OR000003',
                        3,
                        '26/8/2022',
                        '8:42',
                        0,
                        55.0,
                        60,
                        5,
                        'C',
                        'S01',
                        'EM000003',
                        NULL
                    );

INSERT INTO [order] (
                        order_id,
                        order_queue,
                        order_date,
                        order_time,
                        [order_discount (%)],
                        order_total,
                        order_recieved,
                        order_change,
                        order_payment,
                        store_id,
                        employee_id,
                        customer_id
                    )
                    VALUES (
                        'OR000004',
                        4,
                        '26/8/2022',
                        '10:31',
                        5,
                        152.0,
                        200,
                        48,
                        'C',
                        'S01',
                        'EM000004',
                        'CM000003'
                    );


-- Table: order_item
DROP TABLE IF EXISTS order_item;

CREATE TABLE order_item (
    order_item_id     INTEGER      PRIMARY KEY
                                   UNIQUE
                                   NOT NULL,
    order_item_amount INTEGER      NOT NULL,
    order_item_price  INTEGER      NOT NULL,
    order_item_total  INTEGER      NOT NULL,
    product_id        VARCHAR (6)  REFERENCES product (product_id) ON DELETE CASCADE
                                                                   ON UPDATE CASCADE,
    order_id          VARCHAR (10) REFERENCES [order] (order_id) ON DELETE CASCADE
                                                                 ON UPDATE CASCADE
);

INSERT INTO order_item (
                           order_item_id,
                           order_item_amount,
                           order_item_price,
                           order_item_total,
                           product_id,
                           order_id
                       )
                       VALUES (
                           1,
                           1,
                           55,
                           55,
                           'P00004',
                           'OR000001'
                       );

INSERT INTO order_item (
                           order_item_id,
                           order_item_amount,
                           order_item_price,
                           order_item_total,
                           product_id,
                           order_id
                       )
                       VALUES (
                           2,
                           1,
                           35,
                           35,
                           'P00006',
                           'OR000001'
                       );

INSERT INTO order_item (
                           order_item_id,
                           order_item_amount,
                           order_item_price,
                           order_item_total,
                           product_id,
                           order_id
                       )
                       VALUES (
                           3,
                           2,
                           35,
                           70,
                           'P00003',
                           'OR000002'
                       );

INSERT INTO order_item (
                           order_item_id,
                           order_item_amount,
                           order_item_price,
                           order_item_total,
                           product_id,
                           order_id
                       )
                       VALUES (
                           4,
                           1,
                           55,
                           55,
                           'P00004',
                           'OR000002'
                       );

INSERT INTO order_item (
                           order_item_id,
                           order_item_amount,
                           order_item_price,
                           order_item_total,
                           product_id,
                           order_id
                       )
                       VALUES (
                           5,
                           1,
                           55,
                           55,
                           'P00007',
                           'OR000003'
                       );

INSERT INTO order_item (
                           order_item_id,
                           order_item_amount,
                           order_item_price,
                           order_item_total,
                           product_id,
                           order_id
                       )
                       VALUES (
                           6,
                           1,
                           55,
                           55,
                           'P00005',
                           'OR000004'
                       );

INSERT INTO order_item (
                           order_item_id,
                           order_item_amount,
                           order_item_price,
                           order_item_total,
                           product_id,
                           order_id
                       )
                       VALUES (
                           7,
                           3,
                           35,
                           105,
                           'P00006',
                           'OR000004'
                       );


-- Table: product
DROP TABLE IF EXISTS product;

CREATE TABLE product (
    product_id    VARCHAR (6) PRIMARY KEY
                              UNIQUE
                              NOT NULL,
    product_name  TEXT (50)   NOT NULL,
    product_type  VARCHAR (5) DEFAULT HCS
                              NOT NULL,
    product_size  VARCHAR (5) NOT NULL
                              DEFAULT SML,
    product_price INTEGER     NOT NULL,
    category_id   VARCHAR (5) NOT NULL
                              REFERENCES category (category_id) ON DELETE CASCADE
                                                                ON UPDATE CASCADE
);

INSERT INTO product (
                        product_id,
                        product_name,
                        product_type,
                        product_size,
                        product_price,
                        category_id
                    )
                    VALUES (
                        'P00001',
                        'เอสเพรสโซ่',
                        'HCS',
                        'SML',
                        49,
                        'C01'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_type,
                        product_size,
                        product_price,
                        category_id
                    )
                    VALUES (
                        'P00002',
                        'ชาเขียว',
                        'CS',
                        'ML',
                        55,
                        'C02'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_type,
                        product_size,
                        product_price,
                        category_id
                    )
                    VALUES (
                        'P00003',
                        'คุกกี้ธัญพืช',
                        '-',
                        '-',
                        35,
                        'C06'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_type,
                        product_size,
                        product_price,
                        category_id
                    )
                    VALUES (
                        'P00004',
                        'ลาเต้',
                        'CS',
                        'ML',
                        55,
                        'C01'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_type,
                        product_size,
                        product_price,
                        category_id
                    )
                    VALUES (
                        'P00005',
                        'น้ำแอปเปิ้ล',
                        'CS',
                        'ML',
                        55,
                        'C03'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_type,
                        product_size,
                        product_price,
                        category_id
                    )
                    VALUES (
                        'P00006',
                        'ครัวซองต์',
                        '-',
                        '-',
                        35,
                        'C06'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_type,
                        product_size,
                        product_price,
                        category_id
                    )
                    VALUES (
                        'P00007',
                        'อเมริกาโน่',
                        'HC',
                        'SML',
                        55,
                        'C01'
                    );


-- Table: store
DROP TABLE IF EXISTS store;

CREATE TABLE store (
    store_id      VARCHAR (4) PRIMARY KEY
                              NOT NULL
                              UNIQUE,
    store_name    TEXT (12)   NOT NULL,
    store_address TEXT (50)   NOT NULL,
    store_tel     CHAR (10)   NOT NULL
);

INSERT INTO store (
                      store_id,
                      store_name,
                      store_address,
                      store_tel
                  )
                  VALUES (
                      'S01',
                      'D-Coffee',
                      'อาคารวิทยาศาสตร์การแพทย์ คณะสหเวชศาสตร์ ม.บูรพา',
                      '0953325290'
                  );


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
